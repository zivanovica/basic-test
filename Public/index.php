<!DOCTYPE html>
<html>
<head>
    <title>Basic Test</title>
    <script src="Public/Assets/js/libs/jquery-3.2.1.min.js"></script>
    <script src="Public/Assets/js/libs/jquery-migrate-1.4.1.min.js"></script>
    <script src="Public/Assets/js/libs/jquery.loadTemplate.min.js"></script>
    <script src="Public/Assets/js/libs/js.cookie.js"></script>
    <link href="Public/Assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="Public/Assets/css/bootstrap-grid.min.css" rel="stylesheet">
    <link href="Public/Assets/css/bootstrap-reboot.min.css" rel="stylesheet">
    <script src="Public/Assets/js/app.js"></script>
</head>
<body>
<div class="container">
    <ul class="nav" id="navigation_view">
    </ul>
    <div id="notifications"></div>
    <div class="col-md-8 col-md-offset-2">

        <div id="view"></div>
    </div>
</div>


</body>
</html>