(function () {

    var token, user;

    var Notifications = (function () {

        var _notifications = [];

        var addNotification = function (message, type) {

            type = type || 'info';

            var notification = {
                notificationMessage: message,
                notificationType: type
            };

            _notifications.push(notification);

            setTimeout(function () {

                _notifications.splice(_notifications.indexOf(notification), 1);

                renderNotifications();
            }, 2000);

            renderNotifications();
        };

        var renderNotifications = function () {

            $('#notifications').loadTemplate('Public/Templates/notification.html', _notifications);
        };

        return {

            show: addNotification
        };
    })();

    // ======================== VIEW SHOWING HANDLERS ==================================================

    var showRegisterScreen = function () {

        $('#navigation_view').loadTemplate('Public/Templates/logged_out_navigation.html');

        $('#view').loadTemplate('Public/Templates/register.html');
    };

    var showLoginScreen = function () {

        $('#navigation_view').loadTemplate('Public/Templates/logged_out_navigation.html');

        $('#view').loadTemplate('Public/Templates/login.html');
    };

    var showSearchPage = function () {

        $('#navigation_view').loadTemplate('Public/Templates/logged_in_navigation.html');

        $('#view').loadTemplate('Public/Templates/search.html', {
            name: user.name || 'Private',
            email: user.email || 'Private'
        });
    };

    // ============== END OF VIEW SHOWING HANDLERS ===================

    var validateToken = function (validateToken) {

        $.ajax({
            url: 'Api/?_route=/user/get/' + validateToken,
            success: function (response) {

                var data = response.data || {};

                if (data.user) {

                    user = data.user;
                    token = validateToken;

                    showSearchPage();
                }
            },
            error: function () {

                token = null;

                Cookies.remove('token');

                showLoginScreen();
            }
        });

    };

    // ================ EVENT BINDING ===============================

    $(document).on('submit', 'form#register_form', function (event) {

        event.preventDefault();
        event.stopImmediatePropagation();

        $.ajax({
            url: 'Api/?_route=/register',
            type: 'POST',
            data: $(this).serialize(),
            success: function (response) {

                var data = response.data || {};

                if (data.message) {

                    Notifications.show(data.message, 'success');

                    showLoginScreen();
                }

            }.bind(this),
            error: function (xhr) {

                var response = xhr.responseJSON || {};
                var data = response.data || {};

                if (data.message) {

                    Notifications.show(data.message, 'error');
                }

                if (typeof data['_request.validate'] !== 'undefined') {

                    delete data['_request.validate'];

                    var errorFields = Object.keys(data || {});

                    errorFields.forEach(function (field) {

                        var error = data[field][Object.keys(data[field] || {})[0]];

                        $('[name="' + field + '"]')
                            .val('')
                            .attr('placeholder', error)
                            .addClass('error');
                    });
                }
            }
        })

    });

    $(document).on('submit', 'form#login_form', function (event) {

        event.preventDefault();
        event.stopImmediatePropagation();

        $.ajax({
            url: '/Api/?_route=/login',
            type: 'POST',
            data: $(this).serialize(),
            success: function (response) {

                var data = response && response.data ? response.data : {};

                if (data.token) {

                    Cookies.set('token', data.token);

                    validateToken(data.token);
                }
            },
            error: function (xhr) {

                var response = xhr.responseJSON || {};
                var data = response.data || {};

                if (data.message) {

                    Notifications.show(data.message, 'error');
                }

                if (typeof data['_request.validate'] !== 'undefined') {

                    delete data['_request.validate'];

                    var errorFields = Object.keys(data || {});

                    errorFields.forEach(function (field) {

                        var error = data[field][Object.keys(data[field] || {})[0]];

                        $('[name="' + field + '"]')
                            .val('')
                            .attr('placeholder', error)
                            .addClass('error');
                    });
                }
            }
        });
    });

    $(document).on('submit', 'form#search_form', function (event) {

        event.preventDefault();
        event.stopImmediatePropagation();

        var $input = $(this).find('input');

        var term = $input.val() || '';

        if (0 === term.length) {

            $input.attr('placeholder', 'Enter Search Term').addClass('error');

            return;
        }

        $.ajax({
            url: '/Api/?_route=/search/' + token + '/' + term,
            type: 'GET',
            data: $(this).serialize(),
            success: function (response) {

                var data = response && response.data ? response.data : {};

                if (data.results instanceof Array) {

                    if (data.results.length) {

                        $('#results_view').loadTemplate('Public/Templates/result.html', data.results);
                    } else {

                        $('#results_view').loadTemplate('Public/Templates/result.html', {title: '0 results found'});
                    }
                }
            },
            error: function (xhr) {

                $('#results_view').loadTemplate('Public/Templates/result.html', {title: '0 results found'});

                var response = xhr.responseJSON || {};
                var data = response.data || {};

                if (data.message) {

                    Notifications.show(data.message, 'error');
                }

                if (typeof data['_request.validate'] !== 'undefined') {

                    delete data['_request.validate'];

                    var errorFields = Object.keys(data || {});

                    errorFields.forEach(function (field) {

                        var error = data[field][Object.keys(data[field] || {})[0]];

                        $('[name="' + field + '"]')
                            .val('')
                            .attr('placeholder', error)
                            .addClass('error');
                    });
                }
            }
        });
    });

    $(document).on('click', 'a#logout_link', function (event) {

        event.preventDefault();
        event.stopImmediatePropagation();

        // Cookies.remove('token');

        $.ajax({
            url: 'Api/?_route=/logout/' + token,
            type: 'GET',
            success: function (response) {

                token = null;

                Cookies.remove('token');

                showLoginScreen();
            },
            error: function (xhr) {

                var response = xhr.responseJSON || {};
                var data = response.data || {};

                if (data.message) {

                    Notifications.show(data.message, 'error');
                }
            }
        });

        showLoginScreen();
    });

    $(document).on('click', 'a#login_link', function (event) {

        event.preventDefault();
        event.stopImmediatePropagation();

        if (null === token) {

            showLoginScreen();
        }
    });

    $(document).on('click', 'a#register_link', function (event) {

        event.preventDefault();
        event.stopImmediatePropagation();

        if (null === token) {

            showRegisterScreen();
        }
    });

    // ========================== END OF EVENT BINDINGS ====================================

    // ====== INITIALIZATION =================

    $(document).ready(function () {

        token = Cookies.get('token') || null;

        if (null === token) {

            showLoginScreen();
        } else {

            validateToken(token);
        }

    });

    // ========= END OF INITIALIZATION ================

})();
