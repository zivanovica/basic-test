<?php

namespace Api\Controllers\User;

use Api\Models\User;
use RequestHandler\Modules\Application\ApplicationRequest\IFilter;
use RequestHandler\Modules\Application\ApplicationRequest\IHandle;
use RequestHandler\Modules\Application\ApplicationRequest\IValidate;
use RequestHandler\Modules\Request\IRequest;
use RequestHandler\Modules\Request\RequestFilter\IRequestFilter;
use RequestHandler\Modules\Response\IResponse;
use RequestHandler\Modules\Response\IResponseStatus;
use RequestHandler\Utils\DataFilter\Filters\ModelFilter;
use RequestHandler\Utils\Factory\Factory;
use RequestHandler\Utils\InputValidator\IInputValidator;

class Login implements IHandle, IValidate, IFilter
{

    /**
     *
     * Executes when related action is requested
     *
     * @param IRequest $request
     * @param IResponse $response
     * @return IResponse
     */
    public function handle(IRequest $request, IResponse $response): IResponse
    {

        // "login_email" is re-formatted to User model. Look "filter" method of this class
        /** @var User $user */
        $user = $request->get('login_email');

        if (false === $user->exists()) {

            return $response->status(IResponseStatus::NOT_FOUND)->errors(['message' => 'Invalid user email.']);
        }

        if (false === password_verify($request->get('login_password'), $user->getAttribute('password'))) {

            return $response->status(IResponseStatus::UNAUTHORIZED)->errors(['message' => 'Invalid credentials']);
        }

        $token = $user->createNewToken();

        if (null === $token) {

            return $response;
        }

        return $response->data(['token' => $token->getAttribute('token')]);
    }

    /**
     *
     * Validator is used to perform simple request input validations
     * This is executed before middlewares and provides simple way of validating request input before doing anything else.
     *
     *
     * @param IInputValidator $validator
     * @return IInputValidator
     */
    public function validate(IInputValidator $validator): IInputValidator
    {

        return $validator->validate([
            'login_email' => 'required|email',
            'login_password' => 'required'
        ]);
    }

    /**
     *
     * Request filter used to transform given fields to specified types
     *
     * @param IRequestFilter $filter
     * @return IRequestFilter
     */
    public function filter(IRequestFilter $filter): IRequestFilter
    {

        return $filter->add(
            // Tell script to get user model based on "login_email" input from request.
            'login_email', Factory::createNew(ModelFilter::class, User::class, 'email')
        );
    }
}