<?php

namespace Api\Controllers\User;

use Api\Models\User;
use RequestHandler\Modules\Application\ApplicationRequest\IHandle;
use RequestHandler\Modules\Application\ApplicationRequest\IValidate;
use RequestHandler\Modules\Request\IRequest;
use RequestHandler\Modules\Response\IResponse;
use RequestHandler\Modules\Response\IResponseStatus;
use RequestHandler\Utils\InputValidator\IInputValidator;

class Register implements IHandle, IValidate
{

    /**
     *
     * Executes when related action is requested
     *
     * @param IRequest $request
     * @param IResponse $response
     * @return IResponse
     */
    public function handle(IRequest $request, IResponse $response): IResponse
    {

        $user = new User([
            'name' => $request->get('register_name'),
            'password' => password_hash($request->get('register_password'), PASSWORD_BCRYPT),
            'email' => $request->get('register_email')
        ]);

        if ($user->save()) {

            return $response->data(['message' => 'Registration was a success']);
        }

        return $response->status(IResponseStatus::INTERNAL_ERROR)->data(['message' => 'Failed to register user']);
    }

    /**
     *
     * Validator is used to perform simple request input validations
     * This is executed before middlewares and provides simple way of validating request input before doing anything else.
     *
     *
     * @param IInputValidator $validator
     * @return IInputValidator
     */
    public function validate(IInputValidator $validator): IInputValidator
    {

        return $validator->validate([
            'register_name' => 'required|max:64',
            'register_email' => 'email|unique:users,email',
            'register_password' => 'required|min:6|same:register_repeat_password',
            'register_repeat_password' => 'required|min:6|same:register_password'
        ]);
    }
}