<?php

namespace Api\Controllers\User;

use Api\Middlewares\Authenticate;
use Api\Models\Token;
use RequestHandler\Modules\Application\ApplicationRequest\IFilter;
use RequestHandler\Modules\Application\ApplicationRequest\IHandle;
use RequestHandler\Modules\Application\ApplicationRequest\IMiddleware;
use RequestHandler\Modules\Middleware\IMiddlewareContainer;
use RequestHandler\Modules\Request\IRequest;
use RequestHandler\Modules\Request\RequestFilter\IRequestFilter;
use RequestHandler\Modules\Response\IResponse;
use RequestHandler\Modules\Response\IResponseStatus;
use RequestHandler\Utils\DataFilter\Filters\ModelFilter;
use RequestHandler\Utils\Factory\Factory;

class Logout implements IHandle, IMiddleware, IFilter
{

    /**
     *
     * Executes when related action is requested
     *
     * @param IRequest $request
     * @param IResponse $response
     * @return IResponse
     */
    public function handle(IRequest $request, IResponse $response): IResponse
    {
        /** @var Token $token */
        $token = $request->get('token');

        if ($token->delete()) {

            return $response->status(IResponseStatus::OK)->data(['message' => 'Session destroyed.']);
        }

        return $response->status(IResponseStatus::INTERNAL_ERROR)->data(['message' => 'Error removing token']);
    }

    /**
     *
     * Request filter used to transform given fields to specified types
     *
     * @param IRequestFilter $filter
     * @return IRequestFilter
     */
    public function filter(IRequestFilter $filter): IRequestFilter
    {

        return $filter->add(
            'token', Factory::createNew(ModelFilter::class, Token::class, 'token')
        );
    }

    /**
     *
     * Used to register all middlewares that should be executed before handling acton
     *
     * @param IMiddlewareContainer $middleware
     * @return IMiddlewareContainer
     */
    public function middleware(IMiddlewareContainer $middleware): IMiddlewareContainer
    {

        return $middleware->add(new Authenticate());
    }
}