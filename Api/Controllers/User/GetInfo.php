<?php

namespace Api\Controllers\User;

use Api\Models\Token;
use RequestHandler\Modules\Application\ApplicationRequest\IFilter;
use RequestHandler\Modules\Application\ApplicationRequest\IHandle;
use RequestHandler\Modules\Application\ApplicationRequest\IValidate;
use RequestHandler\Modules\Request\IRequest;
use RequestHandler\Modules\Request\RequestFilter\IRequestFilter;
use RequestHandler\Modules\Response\IResponse;
use RequestHandler\Modules\Response\IResponseStatus;
use RequestHandler\Utils\DataFilter\Filters\ModelFilter;
use RequestHandler\Utils\Factory\Factory;
use RequestHandler\Utils\InputValidator\IInputValidator;

class GetInfo implements IHandle, IValidate, IFilter
{

    /**
     *
     * Executes when related action is requested
     *
     * @param IRequest $request
     * @param IResponse $response
     * @return IResponse
     */
    public function handle(IRequest $request, IResponse $response): IResponse
    {

        /** @var Token $token */
        $token = $request->get('token');

        if (false === $token->exists()) {

            return $response->status(IResponseStatus::UNAUTHORIZED)->data(['message' => 'Invalid token']);
        }

        $user = $token->getUser();

        if (null === $user || false === $user->exists()) {

            return $response->status(IResponseStatus::NOT_FOUND)->data(['message' => 'Invalid user']);
        }

        return $response->data(['user' => $user]);
    }

    /**
     *
     * Validator is used to perform simple request input validations
     * This is executed before middlewares and provides simple way of validating request input before doing anything else.
     *
     *
     * @param IInputValidator $validator
     * @return IInputValidator
     */
    public function validate(IInputValidator $validator): IInputValidator
    {

        return $validator->validate([
            'token' => 'required'
        ]);
    }

    /**
     *
     * Request filter used to transform given fields to specified types
     *
     * @param IRequestFilter $filter
     * @return IRequestFilter
     */
    public function filter(IRequestFilter $filter): IRequestFilter
    {

        return $filter->add(
            'token', Factory::create(ModelFilter::class, Token::class, 'token')
        );
    }
}