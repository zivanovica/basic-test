<?php

namespace Api\Controllers;

use Api\Middlewares\Authenticate;
use Api\Models\Post;
use RequestHandler\Modules\Application\ApplicationRequest\IHandle;
use RequestHandler\Modules\Application\ApplicationRequest\IMiddleware;
use RequestHandler\Modules\Application\ApplicationRequest\IValidate;
use RequestHandler\Modules\Middleware\IMiddlewareContainer;
use RequestHandler\Modules\Request\IRequest;
use RequestHandler\Modules\Response\IResponse;
use RequestHandler\Utils\InputValidator\IInputValidator;

class Search implements IHandle, IValidate, IMiddleware
{

    /**
     *
     * Executes when related action is requested
     *
     * @param IRequest $request
     * @param IResponse $response
     * @return IResponse
     */
    public function handle(IRequest $request, IResponse $response): IResponse
    {

        return $response->data(['results' => Post::getByTerm($request->get('term'))]);
    }


    /**
     *
     * Validator is used to perform simple request input validations
     * This is executed before middlewares and provides simple way of validating request input before doing anything else.
     *
     *
     * @param IInputValidator $validator
     * @return IInputValidator
     */
    public function validate(IInputValidator $validator): IInputValidator
    {

        return $validator->validate([
            'term' => 'required'
        ]);
    }

    /**
     *
     * Used to register all middlewares that should be executed before handling acton
     *
     * @param IMiddlewareContainer $middleware
     * @return IMiddlewareContainer
     */
    public function middleware(IMiddlewareContainer $middleware): IMiddlewareContainer
    {

        return $middleware->add(new Authenticate());
    }
}