<?php
/**
 * Created by IntelliJ IDEA.
 * User: coaps
 * Date: 2.10.2017.
 * Time: 00.52
 */

namespace Api\Middlewares;

use Api\Models\Token;
use RequestHandler\Modules\Middleware\IMiddlewareContainer;
use RequestHandler\Modules\Middleware\IMiddlewareHandler;
use RequestHandler\Modules\Request\IRequest;
use RequestHandler\Modules\Response\IResponse;
use RequestHandler\Modules\Response\IResponseStatus;
use RequestHandler\Utils\DataFilter\Filters\ModelFilter;
use RequestHandler\Utils\Factory\Factory;

class Authenticate implements IMiddlewareHandler
{

    /**
     *
     * Execute method for current middleware
     *
     * @param IRequest $request
     * @param IResponse $response
     * @param IMiddlewareContainer $middleware Used to call "next" method
     */
    public function handle(IRequest $request, IResponse $response, IMiddlewareContainer $middleware): void
    {

        /** @var Token $token */
        $token = $request->get(
            'token',
            null,
            Factory::createNew(ModelFilter::class, Token::class, 'token')
        );

        if ($token->exists() && false === $token->hasExpired()) {

            $middleware->next();

            return;
        }

        if ($token->exists()) {

            $token->delete();
        }

        $response->status(IResponseStatus::UNAUTHORIZED)->errors(['message' => 'Invalid token. Please login.']);
    }
}