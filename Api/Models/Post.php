<?php

namespace Api\Models;


use RequestHandler\Modules\Database\IDatabase;
use RequestHandler\Modules\Model\Model;
use RequestHandler\Utils\Factory\Factory;

class Post extends Model
{

    /**
     * @return string Table/collection name
     */
    public function table(): string
    {

        return 'posts';
    }

    /**
     *
     * List of all available fields (columns)
     *
     * @return array
     */
    public function fields(): array
    {

        return ['id', 'title', 'content', 'created_at'];
    }

    /**
     *
     * Retrieve all posts that contains given term in title or body, and return them as an array
     *
     * @param string $term
     * @return array
     */
    public static function getByTerm(string $term): array
    {

        /** @var IDatabase $db */
        $db = Factory::create(IDatabase::class);

        $term = "%{$term}%";

        $posts = $db->fetchAll(
            'SELECT * FROM `posts` WHERE `title` LIKE ? OR `content` LIKE ?;',
            [$term, $term]
        );

        return is_array($posts) ? $posts : [];
    }
}