<?php

namespace Api\Models;

use RequestHandler\Modules\Model\Model;
use RequestHandler\Utils\DataFilter\Filters\IntFilter;
use RequestHandler\Utils\DataFilter\Filters\ModelFilter;
use RequestHandler\Utils\Factory\Factory;

class Token extends Model
{

    const MAX_TOKEN_LIFETIME = 86400;

    /**
     * @return string Table/collection name
     */
    public function table(): string
    {

        return 'tokens';
    }

    /**
     *
     * List of all available fields (columns)
     *
     * @return array
     */
    public function fields(): array
    {

        return ['id', 'token', 'user_id', 'last_used'];
    }

    /**
     *
     * Retrieve user model using access token
     *
     * @return User|null
     */
    public function getUser(): ?User
    {

        $lastUsed = $this->getAttribute('last_used', Factory::create(IntFilter::class));

        if (time() - $lastUsed > Token::MAX_TOKEN_LIFETIME) {

            $this->delete();

            return null;
        }

        // Update token time
        $this->setAttribute('last_used', time())->save();

        return $this->getAttribute(
            'user_id',
                Factory::createNew(ModelFilter::class, User::class)
        );
    }

    /**
     *
     * Checks wether or not token expired
     *
     * @return bool
     */
    public function hasExpired(): bool
    {

        $lastUsed = $this->getAttribute('last_used', Factory::create(IntFilter::class));

        return time() - $lastUsed > Token::MAX_TOKEN_LIFETIME;
    }

    /**
     *
     * Changes current token user and returns instance if save was success
     *
     * @param User $user
     * @return Token|null
     */
    public static function createUserToken(User $user): ?Token
    {

        $token = new Token([
            'token' => hash('sha256', uniqid(uniqid(), true)),
            'last_used' => time(),
            'user_id' => $user->primaryValue(),
        ]);

        if (false === $token->save()) {

            return null;
        }

        return $token;
    }
}