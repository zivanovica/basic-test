<?php

namespace Api\Models;

use RequestHandler\Modules\Model\Model;
use RequestHandler\Utils\DataFilter\Filters\IntFilter;
use RequestHandler\Utils\Factory\Factory;

class User extends Model
{

    protected $_hidden = ['password'];

    /**
     * @return string Table/collection name
     */
    public function table(): string
    {

        return 'users';
    }

    /**
     *
     * List of all available fields (columns)
     *
     * @return array
     */
    public function fields(): array
    {

        return ['id', 'email', 'password', 'name'];
    }

    /**
     *
     * Create new token for current user
     *
     * @return Token|null
     */
    public function createNewToken(): ?Token
    {

        return Token::createUserToken($this);
    }
}