<?php

require_once __DIR__ . '/../vendor/autoload.php';

use RequestHandler\Modules\Application\IApplication;
use RequestHandler\Modules\Router\IRouter;
use RequestHandler\Utils\Factory\Factory;

/** @var IApplication $app */
$app = Factory::create(IApplication::class, __DIR__ . '/config.json');

$app->boot(function (IRouter $router) {

    $router
        ->get('/home', \Api\Controllers\Home::class)
        ->get('/search/:token/:term', \Api\Controllers\Search::class)
        ->get('/user/get/:token', \Api\Controllers\User\GetInfo::class)
        ->get('/logout/:token', \Api\Controllers\User\Logout::class)
        ->post('/register', \Api\Controllers\User\Register::class)
        ->post('/login', \Api\Controllers\User\Login::class);
});