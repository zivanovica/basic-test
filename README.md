Requirements:
    
    - PHP 7.1+
    - MySQL 5.6+
    - Composer


Installation:

    - cd /path/to/project
    - composer install
    - php -S localhost:1024 -t ./
    
Execute `install.sql` to create database and populate data required for search.

Configure `/path/to/project/Api/config.json` to set valid database configuration
    
**NOTE: This was tested using only PHP bulit-in web server, please do same and follow steps while testing this.**

Use "bye" or "world" keywords as example for testing

For this test I used custom framework that I wrote by my self.
Please consider it as well while evaluating this task, thanks.

Link to framework is: https://bitbucket.org/coapsy/actionhandler